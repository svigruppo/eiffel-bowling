#!/bin/bash

set -x
NAME=eiffel
EPASS=eiffelruleznotwithstandingide
tmux new -d -s $NAME "docker run --rm --name $NAME \
  -p 127.0.0.1:6080:6080 \
  -p 127.0.0.1:5900:5900 \
  -e HOST_UID=$(id -u) \
  -e HOST_GID=$(id -g) \
  -e RESOLUT=1600x900 \
  -e VNCPASS=$EPASS \
  -w /home/ubuntu/shared \
  -v $(pwd):/home/ubuntu/shared mmonga/docker-eiffel \
  'startvnc.sh >> /home/ubuntu/.log/vnc.log'"

while ! docker port $NAME >/dev/null; do sleep 1; done
chromium "http://localhost:6080/vnc.html?resize=downscale&autoconnect=1&password=$EPASS"

